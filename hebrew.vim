" Hebrew Points (Unicode) enter with CTRL-K s v for Sh'va etc.
" See also :help digraph
digraph sv 1456 "SheVa
digraph hs 1457 "Hataf Segol
digraph hp 1458 "Hataf Patah
digraph hq 1459 "Hataf Qamats
digraph hr 1460 "HiRiq
digraph tr 1461 "TseRe
digraph sg 1462 "SeGol
digraph pt 1463 "Patah
digraph qm 1464 "QaMats
digraph hl 1465 "HoLam
digraph hh 1466 "Holam Haser for vav
digraph qb 1467 "Qubuts
digraph sr 1468 "Shuruk (Same as Dagesh)
digraph dg 1468 "DaGesh, Mapiq or Shuruk
digraph mt 1469 "MeTeg
digraph rf 1471 "RaFe
digraph hd 1473 "sHin Dot
digraph sd 1474 "Shin Dot
digraph qq 1479 "Qamats Qatan

let keys=["`12345 67890-=", "~!@#$% ^&*()_+", "qwert yuiop[]", "QWERT YUIOP{}", "asdfg hjkl;'\\", 'ASDFG HJKL:"|', "<zxcv bnm,./", ">ZXCV BNM<>?"]
let hebrew=[";12345 67890-=", "~!@#$% ^&*()_+", "/'קרא טוןםפ[]\\", "שדגכע יחלף,", "זסבהנ מצתץ."]
